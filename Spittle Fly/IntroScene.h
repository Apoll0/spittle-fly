//
// Created by SIARHEI TSISHKEVICH on 12.05.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "cocos2d.h"
#import "cocos2d-ui.h"

#define kHighScoreButtonFontSize 24
#define kFlyFullCircleInterval 7.4
#define kButtonScaleFactor 0.5

#define kStartButtonScaleFactor 1
@class MainScene;
@interface IntroScene : CCScene

+ (IntroScene *) scene;
- (id)init;

@end