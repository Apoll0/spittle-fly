//
// Created by SIARHEI TSISHKEVICH on 08.05.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "Score.h"

static const NSString *fileName = @"scores";
@implementation Score
{
    NSInteger _currentScore;
}

- (void)add
{
    _currentScore++;
    if ([self.delegate respondsToSelector:@selector(scoreUpdated:)])
    {
        [self.delegate scoreUpdated:_currentScore];
    }
}

- (void)dec
{
    _currentScore--;
    if ([self.delegate respondsToSelector:@selector(scoreUpdated:)])
    {
        [self.delegate scoreUpdated:_currentScore];
    }
}

- (void)clearCurrentScore
{
    _currentScore = 0;
    if ([self.delegate respondsToSelector:@selector(scoreUpdated:)])
    {
        [self.delegate scoreUpdated:_currentScore];
    }
}

- (NSInteger)currentScore
{
    return _currentScore;
}


- (BOOL)isCurrentScoreTheBest
{
    BOOL isBigger = YES;
    for (int j = 0; j < kMaxHighScores; j++)
    {
        if ([self.scores[j] intValue] >= _currentScore) isBigger = NO;
    }
    return isBigger;
}

- (void)checkHighScoresAndUpdateScores
{
    if ([self isCurrentScoreTheBest])
    {
        for (int j = kMaxHighScores-1; j > 0; j--)
        {
            [self.scores replaceObjectAtIndex:j withObject:self.scores[j-1]];
        }
        [self.scores replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:_currentScore]]; //TODO: Сдвиг
        return;
    }
    for (int j = 0; j < kMaxHighScores-1; j++)
    {
        if (([self.scores[j] intValue] > _currentScore) && (_currentScore > [self.scores[j + 1] intValue]))
        {
            for (int i = kMaxHighScores-1; i > (j + 1); i--)
            {
                [self.scores replaceObjectAtIndex:i withObject:self.scores[i-1]];

            }
            [self.scores replaceObjectAtIndex:(j+1) withObject:[NSNumber numberWithInt:_currentScore]];
            break;
        }
    }
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _currentScore = 0;
        if (![self restoreScoreFromFile])
        {
            self.scores = [[NSMutableArray alloc] initWithCapacity:kMaxHighScores];
            for (int j = 0; j < kMaxHighScores; j++)
            {
                [self.scores addObject:[NSNumber numberWithInt:0]];

            }
            [self saveScoreToFile];
        }
    }

    return self;
}

- (void)saveScoreToFile
{
    BOOL success = [_scores writeToFile:@"scores.plist" atomically:YES];
    NSLog([NSString stringWithFormat:@"%@", success]);
}

- (BOOL)restoreScoreFromFile
{
    _scores = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"]];
    if (_scores) return YES;
    else return NO;
}

@end