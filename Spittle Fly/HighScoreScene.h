//
// Created by SIARHEI TSISHKEVICH on 02.06.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "cocos2d.h"
#import "cocos2d-ui.h"

@class Score;


@interface HighScoreScene : CCScene

@property (strong, nonatomic) Score *score;
+ (HighScoreScene *) scene;
- (id) init;

@end