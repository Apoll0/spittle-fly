//
// Created by SIARHEI TSISHKEVICH on 22.05.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "HowToScene.h"


@implementation HowToScene
{
    CCSprite *bg;
}

#pragma mark - Other methods

#pragma mark - Create & Destroy

+ (HowToScene *)scene
{
    return [[self alloc] init];
}

- (void)onEnter
{
    [super onEnter];
}

- (id)init
{
    self = [super init];
    if (!self) return(nil);
    self.userInteractionEnabled = YES;
    bg = [CCSprite spriteWithImageNamed:@"HowTo.png"];
    bg.anchorPoint = ccp(0,0);
    [self addChild:bg];

    return self;
}
#pragma mark - Touch Handler
// -----------------------------------------------------------------------

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super touchBegan:touch withEvent:event];
    [[CCDirector sharedDirector] popSceneWithTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionRight duration:0.5]];
}
#pragma mark - Buttons callback

@end