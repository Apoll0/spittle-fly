//
// Created by SIARHEI TSISHKEVICH on 11.05.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "cocos2d.h"
#import "cocos2d-ui.h"

@class Score;
#define kScoreFontSize 40.0
#define kReplayButtonFontSize 30.0
#define kMenuButtonFontSize 30.0

@interface FinishScene : CCScene

@property (weak, nonatomic) Score *score;

+ (FinishScene *) scene;
- (id)init;

@end