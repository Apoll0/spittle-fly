//
// Created by SIARHEI TSISHKEVICH on 08.05.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kMaxHighScores 5
@protocol ScoreDelegate;
@interface Score : NSObject
@property (weak, nonatomic) id<ScoreDelegate> delegate;
@property(nonatomic, strong) NSMutableArray *scores;

- (void) add;
- (void) dec;
- (void) clearCurrentScore;
- (NSInteger) currentScore;
- (BOOL) isCurrentScoreTheBest;
- (void) checkHighScoresAndUpdateScores;
- (void) saveScoreToFile;      //TODO: add methods
- (BOOL) restoreScoreFromFile;

@end

@protocol ScoreDelegate <NSObject>
@optional
- (void) scoreUpdated: (NSInteger) newValue;
@end