//
// Created by SIARHEI TSISHKEVICH on 14.05.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCSprite.h"
#import "cocos2d.h"

@protocol FlyDelegate;

#define kFlySpeedDelta 30 // 1/10 sec
#define kFlyScaleFactor 0.3
#define kMovingUp 1
#define kMovingDown -1
//#define kMovingNot 0
//#define kRotateTime 0.2
#define kDelayBeforeRize 0.25
#define kDeadJumpTime 0.5
#define kTimeToBlink 0.8

#define kShakeFlyAmplitude 4
#define kShakeFlyTime 0.08

@interface Fly : CCSprite

@property (weak, nonatomic) id<FlyDelegate> delegate;
- (void)informDelegate;
- (void) killFly;
- (void) moveFlyUp;
- (void) moveFlyDown;
- (void) resetFlyPosition;
- (void) start;
- (void) stop;

@end

@protocol FlyDelegate
@optional

@required
- (void) FlyDeadAndAliveAgain;

@end