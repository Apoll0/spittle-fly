//
// Created by SIARHEI TSISHKEVICH on 02.06.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "HighScoreScene.h"
#import "Score.h"


@implementation HighScoreScene
{
    CCLayoutBox *layoutBox;
}

+ (HighScoreScene *)scene
{
    return [[self alloc] init];
}

- (id)init
{
    self = [super init];
    if (!self) return nil;

    self.userInteractionEnabled = YES;

    CGSize winSize = [[CCDirector sharedDirector] viewSize];
    layoutBox = [[CCLayoutBox alloc] init];
    layoutBox.direction = CCLayoutBoxDirectionVertical;
    layoutBox.spacing = 10.0f;
    layoutBox.position = ccp(winSize.width*0.5, winSize.height*0.3);

    return self;
}

- (void)onEnter
{
    [super onEnter];
    //Modify high score
    [_score checkHighScoresAndUpdateScores];
    for (int j = kMaxHighScores-1; j >0; j--)
    {

        CCLabelTTF *label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d",[_score.scores[j] intValue], nil]
                                               fontName:@"Helvetica"
                                               fontSize:20.0];
        label.color = [CCColor blueColor];
        [layoutBox addChild:label];
    }
    CCLabelTTF *label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d",[_score.scores[0] intValue], nil]
                                           fontName:@"Helvetica"
                                           fontSize:30.0];
    label.color = [CCColor redColor];
    [layoutBox addChild:label];
    [self addChild:layoutBox];
}

#pragma mark - Touch Handler
// -----------------------------------------------------------------------

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super touchBegan:touch withEvent:event];
    [[CCDirector sharedDirector] popSceneWithTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionLeft duration:0.5]];
}

@end