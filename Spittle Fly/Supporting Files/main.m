//
//  main.m
//  Spittle Fly
//
//  Created by SIARHEI TSISHKEVICH on 08.05.14.
//  Copyright SIARHEI TSISHKEVICH 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
        return retVal;
    }
}
