//
// Created by SIARHEI TSISHKEVICH on 12.05.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "IntroScene.h"
#import "MainScene.h"
#import "HowToScene.h"
#import "HighScoreScene.h"


@implementation IntroScene
{
    CCButton *_startButton;
    CCButton *_highScoreButton;
    CCButton *_bgButton;
    CCButton *_soundButton;
    CCButton *_howToButton;
    CCSprite *_fly;
    CGSize _winSize;
    Score *_score;
}
#pragma mark - Other methods

- (NSInteger) getNewRandomMove
{
    NSInteger newRandomMove = arc4random_uniform(200)-100; //TODO: Remove Hardcoded
    return newRandomMove;
}

-(void) moveFly
{
    CCActionMoveTo *moveUP = [CCActionMoveTo actionWithDuration:1.5 position:ccp(_winSize.width-_fly.contentSize.width* kFlyScaleFactor/2,_winSize.height-_fly.contentSize.height* kFlyScaleFactor/2+[self getNewRandomMove])];
    CCActionRotateBy *rotateLeft = [CCActionRotateBy actionWithDuration:0.1 angle:-90];
    CCActionMoveTo *moveLeft = [CCActionMoveTo actionWithDuration:2.0 position:ccp(_fly.contentSize.width* kFlyScaleFactor/2+[self getNewRandomMove],_winSize.height-_fly.contentSize.height* kFlyScaleFactor/2)];
    CCActionRotateBy *rotateDown = [CCActionRotateBy actionWithDuration:0.1 angle:-90];
    CCActionMoveTo *moveDown = [CCActionMoveTo actionWithDuration:1.5 position:ccp(_fly.contentSize.width* kFlyScaleFactor/2,_fly.contentSize.height* kFlyScaleFactor/2+[self getNewRandomMove])];
    CCActionRotateBy *rotateRight = [CCActionRotateBy actionWithDuration:0.1 angle:-90];
    CCActionMoveTo *moveRight = [CCActionMoveTo actionWithDuration:2.0 position:ccp(_winSize.width-_fly.contentSize.width* kFlyScaleFactor/2+[self getNewRandomMove], _fly.contentSize.height* kFlyScaleFactor/2)];
    CCActionRotateBy *rotateUp = [CCActionRotateBy actionWithDuration:0.1 angle:-90];
    CCActionSequence *sequence = [CCActionSequence actions:moveUP, rotateLeft, moveLeft, rotateDown, moveDown, rotateRight, moveRight, rotateUp, nil];
    [_fly runAction:sequence];
}


#pragma mark - Create & Destroy

+ (IntroScene *)scene
{
    return [[self alloc] init];
}

- (void)onEnter
{
    [super onEnter];

    [self resetFlyPosition];
    [self schedule:@selector(moveFly) interval:kFlyFullCircleInterval];
}

-(void)onExit
{
    [self unscheduleAllSelectors];
    [super onExit];
}

- (id)init
{
    self = [super init];
    if (!self) return(nil);

    _winSize = [[CCDirector sharedDirector] viewSize];
    _score = [[Score alloc] init];

    //Init color BG
    [self addChild:[CCNodeColor nodeWithColor:[CCColor colorWithRed:0.41f green:0.83f blue:0.85f]]];

    //Init fly
    _fly = [CCSprite spriteWithImageNamed:@"Fly.png"];
    _fly.scale = kFlyScaleFactor;
    [self resetFlyPosition];
    [self addChild:_fly];

    //Init buttons

    _startButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"StartButton.png"]];
    _startButton.scale = kStartButtonScaleFactor;
    //_startButton.color = [CCColor whiteColor];
    _startButton.position = ccp(_winSize.width/2,_winSize.height*0.7);
    [_startButton setTarget:self selector:@selector(startButtonPressed)];
    [self addChild:_startButton];

    _highScoreButton = [CCButton buttonWithTitle:@"High Score" fontName:@"Chalkduster" fontSize:kHighScoreButtonFontSize];
    _highScoreButton.color = [CCColor blackColor];
    _highScoreButton.position = ccp(_winSize.width/2,_winSize.height/6*2);
    [_highScoreButton setTarget:self selector:@selector(highScoreButtonPressed)];
    [self addChild:_highScoreButton];

    _howToButton = [CCButton buttonWithTitle:@"How to play" fontName:@"Chalkduster" fontSize:kHighScoreButtonFontSize];
    _howToButton.color = [CCColor blackColor];
    _howToButton.position = ccp(_winSize.width/2,_winSize.height/6);
    [_howToButton setTarget:self selector:@selector(howToButtonPressed)];
    [self addChild:_howToButton];

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"BGMusic"])
    {
        _bgButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"ButtonMusOn.png"]];
    } else
    {
        _bgButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"ButtonMusOff.png"]];
    }
    _bgButton.position = ccp(_winSize.width/5,_winSize.height/2);
    [_bgButton setTarget:self selector:@selector(bgButtonPressed)];
    _bgButton.scale = kButtonScaleFactor;
    [self addChild:_bgButton];

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Sound"])
    {
        _soundButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"ButtonSoundOn.png"]];
    } else
    {
        _soundButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"ButtonSoundOff.png"]];
    }
    _soundButton.position = ccp(_winSize.width/5*4,_winSize.height/2);
    [_soundButton setTarget:self selector:@selector(soundButtonPressed)];
    _soundButton.scale = kButtonScaleFactor;
    [self addChild:_soundButton];
    return self;
}

- (void)resetFlyPosition
{
    _fly.position = ccp(_winSize.width- _fly.contentSize.width* kFlyScaleFactor/2, _fly.contentSize.height* kFlyScaleFactor/2);
    _fly.rotation = 0;
}


#pragma mark - Buttons callback

- (void) startButtonPressed
{
    MainScene *scene = [MainScene scene];
    if ([scene respondsToSelector:@selector(setScore:)])
    {
        scene.score = _score;
    }
    [[CCDirector sharedDirector] pushScene:scene withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionDown duration:0.2]];
}


- (void) highScoreButtonPressed       //TODO: High score
{
    HighScoreScene *scene = [HighScoreScene scene];
    if ([scene respondsToSelector:@selector(setScore:)])
    {
        scene.score = _score;
    }
    [[CCDirector sharedDirector] pushScene:scene withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionRight duration:0.5]];
}

- (void) bgButtonPressed
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"BGMusic"])
    {
        [_bgButton setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"ButtonMusOff.png"] forState:CCControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"BGMusic"];
    } else
    {
        [_bgButton setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"ButtonMusOn.png"] forState:CCControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"BGMusic"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void) soundButtonPressed
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Sound"])
    {
        [_soundButton setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"ButtonSoundOff.png"] forState:CCControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Sound"];
    } else
    {
        [_soundButton setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"ButtonSoundOn.png"] forState:CCControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Sound"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) howToButtonPressed
{
    [[CCDirector sharedDirector] pushScene:[HowToScene scene] withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionLeft duration:0.5]];
}
@end