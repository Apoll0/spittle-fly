//
// Created by SIARHEI TSISHKEVICH on 11.05.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#define kScoreText @"Score: "

#import "FinishScene.h"
#import "Score.h"


// -----------------------------------------------------------------------
#pragma mark - FinishScene
// -----------------------------------------------------------------------
@implementation FinishScene
{
    CCLabelTTF *_scoreLabel;
    CCButton *_okButton;
    CCButton *_menuButton;
    CCLayoutBox *layoutBox;
}

#pragma mark - Other methods

#pragma mark - Create & Destroy

+ (FinishScene *)scene
{
    return [[self alloc] init];
}

- (void)onEnter
{
    [super onEnter];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Sound"])
    {
        [[OALSimpleAudio sharedInstance] playEffect:@"insect1.aiff"];
    }

    //Modify high score
    [_score checkHighScoresAndUpdateScores];
    [_score saveScoreToFile];

    for (int j = kMaxHighScores-1; j >0; j--)
    {

        CCLabelTTF *label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d",[_score.scores[j] intValue], nil]
                fontName:@"Helvetica"
                fontSize:20.0];
        label.color = [CCColor blueColor];
        [layoutBox addChild:label];
    }
    CCLabelTTF *label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d",[_score.scores[0] intValue], nil]
                                           fontName:@"Helvetica"
                                           fontSize:30.0];
    label.color = [CCColor redColor];
    [layoutBox addChild:label];
    [self addChild:layoutBox];

    CGSize winSize = [[CCDirector sharedDirector] viewSize];

    //Init score label
    _scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@%d", kScoreText, [self.score currentScore]] fontName:@"Chalkduster" fontSize:kScoreFontSize];
    if ([self.score currentScore] > 0) _scoreLabel.color = [CCColor greenColor];
    else _scoreLabel.color = [CCColor redColor];
    _scoreLabel.position = ccp(winSize.width/4*3, winSize.height/4*3);
    [self addChild:_scoreLabel];
}

- (void)onExit
{
    [layoutBox removeAllChildren];
    [layoutBox removeFromParent];
    [super onExit];
}

- (id)init
{
    self = [super init];
    if (!self) return(nil);

    CGSize winSize = [[CCDirector sharedDirector] viewSize];

    //Init OK button
    _okButton = [CCButton buttonWithTitle:@"Replay" fontName:@"Chalkduster" fontSize:kReplayButtonFontSize];
    _okButton.position = ccp(winSize.width/4*3, winSize.height/2);
    _okButton.color = [CCColor blueColor];
    [_okButton setBackgroundColor:[CCColor lightGrayColor] forState:CCControlStateNormal];
    [_okButton setTarget:self selector:@selector(okButtonClicked)];
    [self addChild:_okButton];

    //Init menu button
    _menuButton = [CCButton buttonWithTitle:@"Menu" fontName:@"Chalkduster" fontSize:kMenuButtonFontSize];
    _menuButton.position = ccp(winSize.width/4*3, winSize.height/4);
    _menuButton.color = [CCColor blueColor];
    [_menuButton setBackgroundColor:[CCColor lightGrayColor] forState:CCControlStateNormal];
    [_menuButton setTarget:self selector:@selector(menuButtonClicked)];
    [self addChild:_menuButton];

    layoutBox = [[CCLayoutBox alloc] init];
    layoutBox.direction = CCLayoutBoxDirectionVertical;
    layoutBox.spacing = 10.0f;
    layoutBox.position = ccp(winSize.width*0.2, winSize.height*0.2);

    return self;
}

#pragma mark - Buttons callback

- (void) okButtonClicked
{
    [self.score clearCurrentScore];
    [[CCDirector sharedDirector] popSceneWithTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionUp duration:0.2]];
}

- (void) menuButtonClicked
{
    [self.score clearCurrentScore];
    [[CCDirector sharedDirector] popToRootScene];
}
@end