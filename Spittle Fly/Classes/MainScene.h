//
//  MainScene.h
//  Spittle Fly
//
//  Created by SIARHEI TSISHKEVICH on 08.05.14.
//  Copyright SIARHEI TSISHKEVICH 2014. All rights reserved.
//
// -----------------------------------------------------------------------

// Importing cocos2d.h and cocos2d-ui.h, will import anything you need to start using Cocos2D v3
#import "cocos2d.h"
#import "cocos2d-ui.h"
#import "Score.h"
#import "Fly.h"

#define kSpittleScaleFactor 0.2
#define kSpittleSpeed 330/1  //Pixels/sec
#define kPlayerRotateSpeed 0.25
#define kPlayerRotateAngle 15
#define kRoundTime 30
//#define kProgressBarDuration 1.0
#define kScoreText @"Score: "

#define kPauseButtonScaleFactor 0.4
#define kStopButtonScaleFactor 0.4
#define kPlayerScalefactor 1.0
#define kSpittleOffset 100
#define kLampScaleFactor 0.4
#define kRoundProgressAddDelta 6
// -----------------------------------------------------------------------
@protocol ScoreDelegate;

@interface MainScene : CCScene <ScoreDelegate, FlyDelegate>

// -----------------------------------------------------------------------

@property(nonatomic, strong) Score *score;

+ (MainScene *)scene;
- (id)init;

// -----------------------------------------------------------------------
@end