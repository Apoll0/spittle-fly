//
//  MainScene.m
//  Spittle Fly
//
//  Created by SIARHEI TSISHKEVICH on 08.05.14.
//  Copyright SIARHEI TSISHKEVICH 2014. All rights reserved.
//
// -----------------------------------------------------------------------


#import "MainScene.h"
#import "FinishScene.h"


// -----------------------------------------------------------------------
#pragma mark - MainScene
// -----------------------------------------------------------------------
@implementation MainScene
{
    CCSprite *_player;
    CCSprite *_spittle;
    Fly *_fly;
    CCLabelTTF *_scoreLabel;
    NSInteger _currentRoundTime;
    CCLabelTTF *_timeLabel;
    NSInteger _currentRoundProgress;
    CGSize _winSize;
    CCButton *_pauseButton;
    BOOL gamePaused;
    CCButton *_stopButton;
    CCNodeColor *_nodeColor;
    CCTexture *_manTexOpen;
    CCTexture *_mantexClose;
    BOOL _doubleSpittleActive;
    CCSprite *_lamp;
}

#pragma mark - Other methods

- (void) changeFadeColor
{
    if (!_nodeColor)
    {
        _nodeColor = [CCNodeColor nodeWithColor:[CCColor grayColor]];
        _nodeColor.opacity = 0.5;
        [self addChild:_nodeColor];
    }
    else
    {
        [_nodeColor removeFromParentAndCleanup:YES];
        _nodeColor = nil;
    }
}

- (void)scoreUpdated:(NSInteger)newValue
{
    if (newValue < 0) _scoreLabel.color = [CCColor redColor];
    else _scoreLabel.color = [CCColor blackColor];

    _scoreLabel.string = [NSString stringWithFormat:@"%@%d", kScoreText, (int)newValue];
}

- (void) movePlayer
{
    _player.texture = _manTexOpen;
    CCActionRotateBy *rotateBy = [CCActionRotateBy actionWithDuration:kPlayerRotateSpeed angle:kPlayerRotateAngle];
    CCActionRotateBy *rotateBack = [CCActionRotateBy actionWithDuration:kPlayerRotateSpeed angle:-kPlayerRotateAngle];
    CCActionCallBlock *texBack = [CCActionCallBlock actionWithBlock:^{_player.texture = _mantexClose;}];
    CCActionSequence *sequence = [CCActionSequence actions:rotateBy, rotateBack, texBack, nil];
    [_player runAction:sequence];
}

- (void)tickProgressTimer
{
    if (_currentRoundProgress <= 0)
    {

        [self finish];
    }
    else
    {
        if (_currentRoundProgress <= 10) _timeLabel.color = [CCColor redColor];
        else _timeLabel.color = [CCColor blueColor];

        _currentRoundProgress--;
        [_timeLabel setString:[NSString stringWithFormat:@"%d", _currentRoundProgress]];
    }
}


- (void)spittleContactFly
{
    if (_doubleSpittleActive)
    {
        //[[OALSimpleAudio sharedInstance] playEffect:@"insect5.wav"];
        _currentRoundProgress += kRoundProgressAddDelta;
        [_timeLabel runAction:[CCActionSequence actionOne:[CCActionScaleBy actionWithDuration:0.1 scale:2]
                                                      two:[CCActionScaleBy actionWithDuration:0.1 scale:0.5]]];
        [_lamp runAction:[CCActionSequence actionOne:[CCActionScaleBy actionWithDuration:0.1 scale:2]
                                                 two:[CCActionScaleBy actionWithDuration:0.1 scale:0.5]]];
    } else
    {
        _doubleSpittleActive = YES;
        _lamp.visible = YES;
        [_lamp runAction:[CCActionSequence actionOne:[CCActionScaleBy actionWithDuration:0.1 scale:2]
                                                 two:[CCActionScaleBy actionWithDuration:0.1 scale:0.5]]];
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Sound"])
    {
        [[OALSimpleAudio sharedInstance] playEffect:@"slap.wav"];
    }

    self.userInteractionEnabled = NO;

    _spittle.visible = NO;
    _spittle.position = ccp(_player.position.x, _player.position.y+ _player.contentSize.height*kPlayerScalefactor/kSpittleOffset);
    [_spittle stopAllActions];
    [self.score add];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Sound"])
    {
        [[OALSimpleAudio sharedInstance] playEffect:@"insectKilled.aiff"];
    }
    [_fly killFly];
}

- (void)FlyDeadAndAliveAgain
{
    self.userInteractionEnabled = YES;
}

#pragma mark - Finish round

- (void) finish
{
    //TODO: Add round finish sound

    FinishScene *finishScene = [FinishScene scene];
    [finishScene setValue:self.score forKey:@"score"];
    [[CCDirector sharedDirector] pushScene:finishScene
                            withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionDown duration:0.2]];
}

// -----------------------------------------------------------------------
#pragma mark - Create & Destroy
// -----------------------------------------------------------------------

+ (MainScene *)scene
{
    return [[self alloc] init];
}

// -----------------------------------------------------------------------

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    _winSize = [[CCDirector sharedDirector] viewSize];

    //Init BG
    //TODO: Add random background
    CCSprite *bg = [CCSprite spriteWithImageNamed:@"BG.png"];
    bg.anchorPoint = ccp(0,0);
    [self addChild:bg];

    //Init lamp
    _lamp = [CCSprite spriteWithImageNamed:@"lamp.png"];
    _lamp.position = ccp(_winSize.width*0.97, _winSize.height/2);
    _lamp.scale = kLampScaleFactor;
    _lamp.visible = NO;
    [self addChild:_lamp];

    // Enable touch handling on scene node
    self.userInteractionEnabled = YES;

    //Init Time label
    _timeLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", _currentRoundTime]
                                    fontName:@"Noteworthy"
                                    fontSize:40.0f];
    _timeLabel.color = [CCColor blueColor];
    _timeLabel.position = ccp(_winSize.width*0.10, _winSize.height*0.95);
    [self addChild:_timeLabel];

    //Init Pause button
    gamePaused = NO;
    _pauseButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"PauseButtonOn.png"]];
    _pauseButton.position = ccp(_winSize.width*0.05, _winSize.height*0.5);
    _pauseButton.scale = kPauseButtonScaleFactor;
    [_pauseButton setTarget:self selector:@selector(pauseButtonPressed)];
    [self addChild:_pauseButton];

    //Init Stop button
    //[_pauseButton setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"PauseButtonOff.png"] forState:CCControlStateNormal];
    _stopButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"BackButton.png"]];
    _stopButton.scale = kStopButtonScaleFactor;
    _stopButton.rotation = 90;
    [_stopButton setTarget:self selector:@selector(stopButtonPressed)];
    _stopButton.position = ccp(_winSize.width*0.05,_winSize.height*0.3);

    //Init score _scoreLabel
    _scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@%d", kScoreText, 0] fontName:@"Chalkduster" fontSize:20.0f];
    _scoreLabel.color = [CCColor blackColor];
    _scoreLabel.anchorPoint = ccp(0,0);
    _scoreLabel.position = ccp(_winSize.width*0.01, _winSize.height*0.01);
    [self addChild:_scoreLabel];

    //Init player
    _manTexOpen = [CCTexture textureWithFile:@"ManOpen.png"];
    _mantexClose = [CCTexture textureWithFile:@"ManClose.png"];
    _player = [CCSprite spriteWithTexture:_mantexClose];
    _player.scale = kPlayerScalefactor;
    _player.position = ccp(_winSize.width* kPlayerScalefactor*0.15+_player.contentSize.width* kPlayerScalefactor/2, _winSize.height/3);
    [self addChild:_player];

    //Init spittle
    _spittle = [CCSprite spriteWithImageNamed:@"Spittle.png"];
    _spittle.visible = NO;
    _spittle.scale = kSpittleScaleFactor;
    _spittle.position = ccp(_player.position.x, _player.position.y+_player.contentSize.height/6);
    [self addChild:_spittle];

    //Init fly
    _fly = [[Fly alloc] init];
    _fly.scale = kFlyScaleFactor;
    _fly.delegate = self;
    [_fly resetFlyPosition];
    [self addChild:_fly];

    return self;
}

// -----------------------------------------------------------------------

- (void)dealloc
{
    // clean up code goes here
}

// -----------------------------------------------------------------------
#pragma mark - Enter & Exit
// -----------------------------------------------------------------------

- (void)onEnter
{
    [super onEnter];
    if (self.score) self.score.delegate = self;

    _doubleSpittleActive = NO;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"BGMusic"])
    {
        [[OALSimpleAudio sharedInstance] preloadBg:@"bg.mp3"];
        [[OALSimpleAudio sharedInstance] playBgWithLoop:YES];
    }

    //Reset Fly
    [_fly start];
}

- (void)onEnterTransitionDidFinish
{
    _currentRoundTime = kRoundTime;
    _currentRoundProgress = _currentRoundTime;
    [self schedule:@selector(tickProgressTimer) interval:1.0];
    [super onEnterTransitionDidFinish];
}
// -----------------------------------------------------------------------

- (void)onExit
{
    if ([[OALSimpleAudio sharedInstance] bgPlaying]) [[OALSimpleAudio sharedInstance] stopBg];
    [self unscheduleAllSelectors];
    [_spittle stopAllActions];
    _spittle.visible = NO;

    [_fly stop];
    [super onExit];
}

// -----------------------------------------------------------------------
#pragma mark - Touch Handler
// -----------------------------------------------------------------------

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super touchBegan:touch withEvent:event];
    if (_spittle.visible) return;

    CGPoint touchLocation = [touch locationInNode:self];
    _spittle.position = ccp(_player.position.x, _player.position.y+_player.contentSize.height/kSpittleOffset);

    int offX = (int)(touchLocation.x - _spittle.position.x);
    int offY = (int)(touchLocation.y - _spittle.position.y);
    if (0 >= offX)return;

    // Определяем направление стрельбы
    int realX = self.contentSize.width;
    float ratio = (float) offY / (float) offX;
    int realY = (realX * ratio) + _spittle.position.y;
    if (realY <= 0) realY = 0;
    else if (realY >= self.contentSize.height) realY = self.contentSize.height;
    CGPoint realDest = ccp(realX, realY);

    // Spittle speed
    int offRealX = realX - _spittle.position.x;
    int offRealY = realY - _spittle.position.y;
    float length = sqrtf((offRealX*offRealX)+(offRealY*offRealY));
    float velocity = kSpittleSpeed;
    float realMoveDuration = length/velocity;

    CCActionMoveTo *throw = [CCActionMoveTo actionWithDuration:realMoveDuration position:realDest];
    CCActionSequence *sequence = [CCActionSequence actionOne:throw
                                                         two:[CCActionCallBlock actionWithBlock:
                                                                 ^{
                                                                     _spittle.visible = NO;
                                                                     _doubleSpittleActive = NO;
                                                                     _lamp.visible = NO;
                                                                     [self.score dec];
                                                                 }]];

    _spittle.visible = YES;
    [self movePlayer];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Sound"])
    {
        [[OALSimpleAudio sharedInstance] playEffect:@"spit.wav"];
    }
    [_spittle runAction:sequence];
}

#pragma mark - Update

- (void)update:(CCTime)delta
{
    if (CGRectIntersectsRect(CGRectInset(_fly.boundingBox, _fly.boundingBox.size.width*0.2, _fly.boundingBox.size.height*0.2), _spittle.boundingBox))
    {
        [self spittleContactFly];
    }
}

#pragma mark - Pause & Resume Methods

- (void)pauseGame
{
    gamePaused = YES;
    [self changeFadeColor];
    _pauseButton.zOrder = 1;
    [self addChild:_stopButton];
    [[CCDirector sharedDirector] pause];
}

- (void)resumeGame
{
    if (gamePaused)
    {
        gamePaused = NO;
        [_pauseButton setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"PauseButtonOn.png"] forState:CCControlStateNormal];
        [_stopButton removeFromParent];
        [self changeFadeColor];
        [[CCDirector sharedDirector] resume];
    }
}

// -----------------------------------------------------------------------
#pragma mark - Button Callbacks
// -----------------------------------------------------------------------
- (void) pauseButtonPressed
{
    if (!gamePaused)
    {
        [self pauseGame];
    }
    else
    {
        [self resumeGame];
    }
}



- (void) stopButtonPressed
{
    [self.score clearCurrentScore];
    [self resumeGame];
    [[CCDirector sharedDirector] popSceneWithTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionUp duration:0.2]];
}
// -----------------------------------------------------------------------
@end
