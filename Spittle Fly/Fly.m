//
// Created by SIARHEI TSISHKEVICH on 14.05.14.
// Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "Fly.h"


@implementation Fly
{
    //CGPoint _lastPosition;
    NSInteger _movingDirection; // -1 - down, 0 - not mobing, 1 - up
    //NSInteger _lastMovingDirection;
    CGSize _winSize;
    BOOL _dead;
    BOOL _active;
    BOOL _isShaked;
}
- (id)init
{
    self = [super initWithImageNamed:@"Fly2.png"];
    if (self)
    {
        //_movingDirection = kMovingUp;
        self.rotation = 0;
        //_lastMovingDirection = kMovingUp;
        //_lastPosition = _position;
        _winSize = [[CCDirector sharedDirector] viewSize];
        _dead = NO;
        _isShaked = NO;
    }

    return self;
}

- (void) shakeFly
{
    if (!_isShaked)           //TODO: Check why doesn't start after Kill
    {
        _isShaked = YES;
        CCActionMoveBy *moveLeft = [CCActionMoveBy actionWithDuration:kShakeFlyTime/3 position:ccp(-kShakeFlyAmplitude/2,0)];
        CCActionMoveBy *moveRight = [CCActionMoveBy actionWithDuration:kShakeFlyTime/3 position:ccp(kShakeFlyAmplitude,0)];
        CCActionMoveBy *moveBack = [CCActionMoveBy actionWithDuration:kShakeFlyTime/3 position:ccp(-kShakeFlyAmplitude/2,0)];
        CCActionCallBlock *callBlock = [CCActionCallBlock actionWithBlock:^{_isShaked = NO;}];
        [self runAction:[CCActionSequence actions:moveLeft, moveRight, moveBack, callBlock, nil]];
    }
}

- (double) getNewSpeedTime
{
    double newSpeedTime = ((double)((arc4random()% kFlySpeedDelta)+1))/10.0+0.2;
    return newSpeedTime;
}

- (void)informDelegate
{
    [self.delegate FlyDeadAndAliveAgain];
}

- (void)resetFlyPosition
{
    if (_movingDirection == kMovingUp)
    {
        self.position = ccp(_winSize.width*0.9, self.contentSize.height*kFlyScaleFactor/2);
        //self.rotation = 0;
    }
    else
    {
        self.position = ccp(_winSize.width*0.9, _winSize.height- self.contentSize.height*kFlyScaleFactor/2);
        //self.rotation = 180;
    }
}

- (void)moveFlyUp
{
    _movingDirection = kMovingUp;
    //if (self.rotation != 0) [self runAction:[CCActionRotateTo actionWithDuration:kRotateTime angle:0]];
    CCActionMoveTo *moveUp = [CCActionMoveTo actionWithDuration:[self getNewSpeedTime] position:ccp(self.position.x,_winSize.height-self.contentSize.height* kFlyScaleFactor/2)];
    CCActionCallBlock *resetMovingDir = [CCActionCallBlock actionWithBlock:^
    {
        //_movingDirection = kMovingNot;
        //_lastMovingDirection = kMovingUp;
    }];
    [self runAction:[CCActionSequence actions: moveUp, resetMovingDir, nil]];
}

- (void)moveFlyDown
{
    //TODO: Add жужжание
    _movingDirection = kMovingDown;
    //if (self.rotation != 180) [self runAction:[CCActionRotateTo actionWithDuration:kRotateTime angle:180]];
    CCActionMoveTo *moveDown = [CCActionMoveTo actionWithDuration:[self getNewSpeedTime] position:ccp(self.position.x,self.contentSize.height* kFlyScaleFactor/2)];
    CCActionCallBlock *resetMovingDir = [CCActionCallBlock actionWithBlock:^
    {
        //_movingDirection = kMovingNot;
        //_lastMovingDirection = kMovingDown;
    }];
    [self runAction:[CCActionSequence actions: moveDown, resetMovingDir, nil]];
}

- (void) killFly
{
    [self stopAllActions];
    _dead = YES;
    CCActionJumpTo *jumpTo = [CCActionJumpTo actionWithDuration:kDeadJumpTime position:ccp(_winSize.width+self.contentSize.width, -self.contentSize.height) height:_winSize.height/3 jumps:1];
    CCActionCallFunc *informDel = [CCActionCallFunc actionWithTarget:self selector:@selector(informDelegate)];
    [self runAction:[CCActionSequence actions:jumpTo,
                                              [CCActionDelay actionWithDuration:kDelayBeforeRize],
                                              [CCActionCallFunc actionWithTarget:self selector:@selector(resetFlyPosition)],
                                              [CCActionBlink actionWithDuration:kTimeToBlink blinks:3],
                                              informDel,
                                              [CCActionCallBlock actionWithBlock:^
                                              {
                                                  if (_movingDirection == kMovingUp)
                                                  {
                                                      _movingDirection = kMovingDown;
                                                  } else
                                                  {
                                                      _movingDirection = kMovingUp;
                                                  }
                                                  _dead = NO;
                                                  _isShaked = NO;
                                              }],
                                              nil]];

}

- (void)update:(CCTime)delta
{
    if (_active)
    {
        if (([self numberOfRunningActions] == 0) && (!_dead))
        {
            if (_movingDirection == kMovingUp) [self moveFlyDown];
            else [self moveFlyUp];
        }
        [self shakeFly];
    }
}

#pragma mark - Start & stop
- (void)start
{
    _movingDirection = kMovingUp;
    //self.rotation = 0;
    //_lastMovingDirection = kMovingDown;
    [self resetFlyPosition];
    [self setVisible:YES];
    [self moveFlyUp];
    _active = YES;
}

- (void)stop
{
    _active = NO;
    [self stopAllActions];
    [self resetFlyPosition];
    [self setVisible:NO];
}

@end